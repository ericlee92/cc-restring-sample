package com.example.restringsampleproject

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.ice.restring.Restring
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var newString: Map<String, String> = mapOf("test_string" to "puki mak")
        Restring.setStrings("en", newString);

        val clickListener = View.OnClickListener {view ->
            val n = randomNum()
            var newString2: Map<String, String> = mapOf("test_string" to "puki mak")
            if (n %2== 0) {
                newString2 = mapOf("test_string" to "defg")
            } else {
                newString2 = mapOf("test_string" to "456")
            }
            Restring.setStrings("en", newString2);
            Toast.makeText(applicationContext,getString(R.string.test_string),Toast.LENGTH_SHORT).show()

        }
        btnTest.setOnClickListener(clickListener)
    }

    private fun randomNum(): Int {
        return Random().nextInt(80 - 65) + 65;

    }

    protected override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(Restring.wrapContext(newBase))
    }
}
