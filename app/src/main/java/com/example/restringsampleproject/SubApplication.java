package com.example.restringsampleproject;

import android.app.Application;

import com.ice.restring.Restring;

public class SubApplication extends Application {
    // Overriding this method is totally optional!
    @Override
    public void onCreate() {
        super.onCreate();
        Restring.init(getApplicationContext());
    }
}